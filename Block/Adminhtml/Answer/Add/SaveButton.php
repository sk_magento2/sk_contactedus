<?php
/**
 * @copyright: Copyright © 2019 Serhiy Kapitanets. All rights reserved.
 * @author   : Serhiy Kapitanets <kapitanets@gmail.com>
 */
namespace SK\ContactedUs\Block\Adminhtml\Answer\Add;

use Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface;

/**
 * Class SaveButton
 * @package SK\ContactedUs\Block\Adminhtml\Answer\Add
 */
class SaveButton extends GenericButton implements ButtonProviderInterface
{
    /**
     * @inheritdoc
     *
     * @return array
     */
    public function getButtonData()
    {
        return [
            'label' => __('Send'),
            'class' => 'save primary',
            'data_attribute' => [
                'mage-init' => ['button' => ['event' => 'save']],
                'form-role' => 'save',
            ],
            'sort_order' => 20,
        ];
    }
}
