<?php
/**
 * @copyright: Copyright © 2019 Serhiy Kapitanets. All rights reserved.
 * @author   : Serhiy Kapitanets <kapitanets@gmail.com>
 */
namespace SK\ContactedUs\Plugin\Magento\Contact\Model;

use Magento\Contact\Model\Mail as ContactMail;
use Magento\Framework\Api\DataObjectHelper;
use SK\ContactedUs\Api\Data\ContactedusInterface;
use SK\ContactedUs\Api\Data\ContactedusInterfaceFactory;
use SK\ContactedUs\Api\ContactedusRepositoryInterface;
use Magento\Framework\Exception\LocalizedException;

/**
 * Class Mail
 * @package SK\ContactedUs\Plugin\Magento\Contact\Model
 */
class Mail
{
    /**
     * @var ContactedusInterfaceFactory
     */
    protected $contactedusDataFactory;

    /**
     * @var ContactedusRepositoryInterface
     */
    protected $contactedusRepository;

    /**
     * @var DataObjectHelper
     */
    protected $dataObjectHelper;

    /**
     * Mail constructor.
     *
     * @param ContactedusInterfaceFactory $contactedusDataFactory
     * @param ContactedusRepositoryInterface $contactedusRepository
     * @param DataObjectHelper $dataObjectHelper
     */
    public function __construct(
        ContactedusInterfaceFactory $contactedusDataFactory,
        ContactedusRepositoryInterface $contactedusRepository,
        DataObjectHelper $dataObjectHelper
    ) {
        $this->contactedusDataFactory = $contactedusDataFactory;
        $this->contactedusRepository = $contactedusRepository;
        $this->dataObjectHelper = $dataObjectHelper;
    }

    /**
     * @param ContactMail $subject
     * @param array $variables
     * @throws LocalizedException
     */
    public function beforeSend(ContactMail $subject, $replyTo, array $variables)
    {
        $contactedusData = [
            ContactedusInterface::NAME => $variables['data']['name'],
            ContactedusInterface::EMAIL => $variables['data']['email'],
            ContactedusInterface::PHONE => $variables['data']['telephone'],
            ContactedusInterface::COMMENT => $variables['data']['comment'],
            ContactedusInterface::STATUS => 1,
        ];

        $contactedusDataObject = $this->contactedusDataFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $contactedusDataObject,
            $contactedusData,
            ContactedusInterface::class
        );

        $this->contactedusRepository->save($contactedusDataObject);
    }
}
