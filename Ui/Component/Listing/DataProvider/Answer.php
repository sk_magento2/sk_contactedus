<?php
/**
 * @copyright: Copyright © 2019 Serhiy Kapitanets. All rights reserved.
 * @author   : Serhiy Kapitanets <kapitanets@gmail.com>
 */
namespace SK\ContactedUs\Ui\Component\Listing\DataProvider;

use Magento\Framework\App\RequestInterface;
use Magento\Ui\DataProvider\AbstractDataProvider;
use SK\ContactedUs\Model\ResourceModel\Answer\CollectionFactory;

/**
 * Class Answer
 * @package SK\ContactedUs\Ui\Component\Listing\DataProvider
 */
class Answer extends AbstractDataProvider
{
    /**
     * @var RequestInterface $request ,
     */
    private $request;

    /**
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param CollectionFactory $collectionFactory
     * @param RequestInterface $request
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $collectionFactory,
        RequestInterface $request,
        array $meta = [],
        array $data = []
    ) {
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
        $this->collection = $collectionFactory->create();
        $this->request = $request;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        $collection = $this->getCollection();
        $data = [];

        $adminNameColumn = 'IFNULL('
            . 'CONCAT(au.firstname, " ", au.lastname),'
            . ' main_table.admin_id)'
            . ' as admin_name';

        $collection->getSelect()->joinLeft(
            ['au' => $collection->getTable('admin_user')],
            'au.user_id = main_table.admin_id',
            [$adminNameColumn]
        );

        if ($this->request->getParam('parent_id')) {
            $collection->addFieldToFilter('parent_id', $this->request->getParam('parent_id'));
            $data = $collection->toArray();
        }

        return $data;
    }
}
