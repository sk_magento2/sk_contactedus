<?php
/**
 * @copyright: Copyright © 2019 Serhiy Kapitanets. All rights reserved.
 * @author   : Serhiy Kapitanets <kapitanets@gmail.com>
 */
namespace SK\ContactedUs\Controller\Adminhtml\Answer;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Controller\Result\Json;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\DataObject;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use SK\ContactedUs\Api\AnswerRepositoryInterface;
use SK\ContactedUs\Api\ContactedusRepositoryInterface;
use SK\ContactedUs\Api\Data\AnswerInterface;
use SK\ContactedUs\Api\Data\AnswerInterfaceFactory;
use SK\ContactedUs\Api\Data\ContactedusInterface;
use SK\ContactedUs\Model\MailInterface;
use Psr\Log\LoggerInterface;

/**
 * Class Save
 * @package SK\ContactedUs\Controller\Adminhtml\Answer
 */
class Save extends Action
{
    public const ADMIN_RESOURCE = 'SK_ContactedUs::answer_save';

    /**
     * @var AnswerInterfaceFactory
     */
    private $answerDataFactory;

    /**
     * @var AnswerRepositoryInterface
     */
    private $answerRepository;

    /**
     * @var ContactedusRepositoryInterface
     */
    private $contactedusRepository;

    /**
     * @var MailInterface
     */
    private $mail;

    /**
     * @var DataObjectHelper
     */
    private $dataObjectHelper;

    /**
     * @var JsonFactory
     */
    private $resultJsonFactory;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * Save constructor.
     *
     * @param Context $context
     * @param AnswerInterfaceFactory $answerDataFactory
     * @param AnswerRepositoryInterface $answerRepository
     * @param ContactedusRepositoryInterface $contactedusRepository
     * @param MailInterface $mail
     * @param DataObjectHelper $dataObjectHelper
     * @param JsonFactory $resultJsonFactory
     * @param LoggerInterface $logger
     */
    public function __construct(
        Context $context,
        AnswerInterfaceFactory $answerDataFactory,
        AnswerRepositoryInterface $answerRepository,
        ContactedusRepositoryInterface $contactedusRepository,
        MailInterface $mail,
        DataObjectHelper $dataObjectHelper,
        JsonFactory $resultJsonFactory,
        LoggerInterface $logger
    ) {
        parent::__construct($context);
        $this->answerDataFactory = $answerDataFactory;
        $this->answerRepository = $answerRepository;
        $this->contactedusRepository = $contactedusRepository;
        $this->mail = $mail;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->logger = $logger;
    }

    /**
     * Save action
     *
     * @return Json
     */
    public function execute()
    {
        $contactedusId = $this->getRequest()->getParam('parent_id', null);
        $comment = $this->getRequest()->getParam('comment', '');

        $error = false;
        try {
            if (trim($comment) === '') {
                throw new LocalizedException(__('Enter the comment and try again.'));
            }

            /** @var ContactedusInterface $contactedus */
            $contactedus = $this->contactedusRepository->getById($contactedusId);

            $answerData = [
                AnswerInterface::PARENT_ID => $contactedus->getId(),
                AnswerInterface::COMMENT => $comment,
                AnswerInterface::ADMIN_ID => '1',
            ];

            $answerDataObject = $this->answerDataFactory->create();
            $this->dataObjectHelper->populateWithArray(
                $answerDataObject,
                $answerData,
                AnswerInterface::class
            );

            $emailData = [
                'name' => $contactedus->getName(),
                'comment' => $answerDataObject->getComment(),
            ];
            $this->sendAnswer($contactedus->getEmail(), $emailData);

            /** @var AnswerInterface $answer */
            $answer = $this->answerRepository->save($answerDataObject);
            $answerId = $answer->getId();

            $message = __('Answer #%1 has been sent!', $answerId);
        } catch (NoSuchEntityException $e) {
            $this->logger->critical($e);
            $error = true;
            $message = __('This Contacted Us no longer exists.');
        } catch (LocalizedException $e) {
            $error = true;
            $message = __($e->getMessage());
            $this->logger->critical($e);
        } catch (\Exception $e) {
            $error = true;
            $message = __('Something went wrong while sending the Answer.');
            $this->logger->critical($e);
        }

        $resultJson = $this->resultJsonFactory->create();
        $resultJson->setData(
            [
                'message' => $message,
                'error' => $error,
            ]
        );

        return $resultJson;
    }

    /**
     * Send Answer Email
     *
     * @param string $to
     * @param array $data
     */
    public function sendAnswer($to, array $data)
    {
        $this->mail->send($to, ['data' => new DataObject($data)]);
    }
}
