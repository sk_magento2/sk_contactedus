<?php
/**
 * @copyright: Copyright © 2019 Serhiy Kapitanets. All rights reserved.
 * @author   : Serhiy Kapitanets <kapitanets@gmail.com>
 */
namespace SK\ContactedUs\Controller\Adminhtml\Contactedus;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Redirect;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use SK\ContactedUs\Api\ContactedusRepositoryInterface;
use SK\ContactedUs\Api\Data\ContactedusInterface;
use Psr\Log\LoggerInterface;

/**
 * Class Save
 * @package SK\ContactedUs\Controller\Adminhtml\Contactedus
 */
class Save extends Action
{
    public const ADMIN_RESOURCE = 'SK_ContactedUs::contactedus_save';

    /**
     * @var ContactedusRepositoryInterface
     */
    private $contactedusRepository;

    /**
     * @var DataObjectHelper
     */
    private $dataObjectHelper;

    /**
     * @var DataPersistorInterface
     */
    private $dataPersistor;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @param Context $context
     * @param ContactedusRepositoryInterface $contactedusRepository
     * @param DataObjectHelper $dataObjectHelper
     * @param DataPersistorInterface $dataPersistor
     * @param LoggerInterface $logger
     */
    public function __construct(
        Context $context,
        ContactedusRepositoryInterface $contactedusRepository,
        DataObjectHelper $dataObjectHelper,
        DataPersistorInterface $dataPersistor,
        LoggerInterface $logger
    ) {
        $this->dataPersistor = $dataPersistor;
        $this->contactedusRepository = $contactedusRepository;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->logger = $logger;
        parent::__construct($context);
    }

    /**
     * Save action
     *
     * @return ResultInterface
     */
    public function execute()
    {
        /** @var Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $data = $this->getRequest()->getPostValue();

        if ($data) {
            $id = $this->getRequest()->getParam('entity_id');
            try {
                /** @var ContactedusInterface $contactedus */
                $contactedus = $this->contactedusRepository->getById($id);
                $this->dataObjectHelper->populateWithArray(
                    $contactedus,
                    $data,
                    ContactedusInterface::class
                );

                $contactedus = $this->contactedusRepository->save($contactedus);
                $this->messageManager->addSuccessMessage(__('You saved the Contacted Us.'));
                $this->dataPersistor->clear('sk_contactedus');
                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/view', ['entity_id' => $contactedus->getId()]);
                }
                return $resultRedirect->setPath('*/*/');
            } catch (NoSuchEntityException $e) {
                $this->logger->critical($e);
                $this->messageManager->addErrorMessage(__('This Contacted Us no longer exists.'));
                //return $resultRedirect->setPath('*/*/');
            } catch (LocalizedException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
                $this->logger->critical($e);
            } catch (\Exception $e) {
                $this->messageManager->addExceptionMessage($e, __('Something went wrong while saving the Contacted Us.'));
                $this->logger->critical($e);
            }

            $this->dataPersistor->set('sk_contactedus', $data);
            return $resultRedirect->setPath('*/*/view', ['entity_id' => $id]);
        }

        return $resultRedirect->setPath('*/*/');
    }
}
