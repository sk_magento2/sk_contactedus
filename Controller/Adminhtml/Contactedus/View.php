<?php
/**
 * @copyright: Copyright © 2019 Serhiy Kapitanets. All rights reserved.
 * @author   : Serhiy Kapitanets <kapitanets@gmail.com>
 */
namespace SK\ContactedUs\Controller\Adminhtml\Contactedus;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Page;
use Magento\Backend\Model\View\Result\Redirect;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\View\Result\PageFactory;
use SK\ContactedUs\Api\ContactedusRepositoryInterface;
use SK\ContactedUs\Api\Data\ContactedusInterface;
use Psr\Log\LoggerInterface;

/**
 * Class View
 * @package SK\ContactedUs\Controller\Adminhtml\Contactedus
 */
class View extends Action
{
    public const ADMIN_RESOURCE = 'SK_ContactedUs::contactedus';

    /**
     * @var ContactedusRepositoryInterface
     */
    private $contactedusRepository;

    /**
     * @var PageFactory
     */
    private $resultPageFactory;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @param Context $context
     * @param ContactedusRepositoryInterface $contactedusRepository
     * @param PageFactory $resultPageFactory
     * @param LoggerInterface $logger
     */
    public function __construct(
        Context $context,
        ContactedusRepositoryInterface $contactedusRepository,
        PageFactory $resultPageFactory,
        LoggerInterface $logger
    ) {
        $this->contactedusRepository = $contactedusRepository;
        $this->resultPageFactory = $resultPageFactory;
        $this->logger = $logger;
        parent::__construct($context);
    }

    /**
     * Edit action
     *
     * @return ResultInterface
     */
    public function execute()
    {
        /** @var Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();

        $id = $this->getRequest()->getParam('entity_id', null);
        if ($id === null) {
            return $resultRedirect->setPath('*/*/');
        }

        try {
            /** @var ContactedusInterface $contactedus */
            $contactedus = $this->contactedusRepository->getById($id);
        } catch (NoSuchEntityException $e) {
            $this->logger->critical($e);
            $this->messageManager->addErrorMessage(__('This Contacted Us no longer exists.'));
            return $resultRedirect->setPath('*/*/');
        } catch (\Exception $e) {
            $this->logger->critical($e);
            $this->messageManager->addErrorMessage(__('Something went wrong while loading Contacted Us.'));
            return $resultRedirect->setPath('*/*/');
        }

        /** @var Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->getConfig()->getTitle()->prepend(__('Contacted Us'));
        $resultPage->getConfig()->getTitle()->prepend(__('Contacted Us View #%1', $contactedus->getId()));

        return $resultPage;
    }
}
