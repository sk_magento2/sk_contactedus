<?php
/**
 * @copyright: Copyright © 2019 Serhiy Kapitanets. All rights reserved.
 * @author   : Serhiy Kapitanets <kapitanets@gmail.com>
 */
namespace SK\ContactedUs\Controller\Adminhtml\Contactedus;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Redirect;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use SK\ContactedUs\Api\ContactedusRepositoryInterface;
use Psr\Log\LoggerInterface;

/**
 * Class Delete
 * @package SK\ContactedUs\Controller\Adminhtml\Contactedus
 */
class Delete extends Action
{
    public const ADMIN_RESOURCE = 'SK_ContactedUs::contactedus_delete';

    /**
     * @var ContactedusRepositoryInterface
     */
    private $contactedusRepository;

    /**
     * @var LoggerInterface
     */
    private $logger;

    public function __construct(
        Context $context,
        ContactedusRepositoryInterface $contactedusRepository,
        LoggerInterface $logger
    ) {
        $this->contactedusRepository = $contactedusRepository;
        $this->logger = $logger;
        parent::__construct($context);
    }

    /**
     * Delete action
     *
     * @return ResultInterface
     */
    public function execute()
    {
        /** @var Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $id = $this->getRequest()->getParam('entity_id');
        try {
            $this->contactedusRepository->deleteById($id);
            $this->messageManager->addSuccessMessage(__('You deleted the Contacted Us.'));
        } catch (NoSuchEntityException $e) {
            $this->logger->critical($e);
            $this->messageManager->addErrorMessage(__('This Contacted Us no longer exists.'));
        } catch (\Exception $e) {
            $this->logger->critical($e);
            $this->messageManager->addErrorMessage($e->getMessage());
            return $resultRedirect->setPath('*/*/edit', ['entity_id' => $id]);
        }

        return $resultRedirect->setPath('*/*/');
    }
}
