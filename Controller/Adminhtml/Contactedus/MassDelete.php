<?php
/**
 * @copyright: Copyright © 2019 Serhiy Kapitanets. All rights reserved.
 * @author   : Serhiy Kapitanets <kapitanets@gmail.com>
 */
namespace SK\ContactedUs\Controller\Adminhtml\Contactedus;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Redirect;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Ui\Component\MassAction\Filter;
use SK\ContactedUs\Api\ContactedusRepositoryInterface;
use SK\ContactedUs\Model\Contactedus;
use SK\ContactedUs\Model\ResourceModel\Contactedus\Collection;
use SK\ContactedUs\Model\ResourceModel\Contactedus\CollectionFactory;
use Psr\Log\LoggerInterface;

/**
 * Class MassDelete
 * @package SK\ContactedUs\Controller\Adminhtml\Contactedus
 */
class MassDelete extends Action
{
    /**
     * Authorization level of a basic admin session
     *
     * @see MassDelete::_isAllowed()
     */
    const ADMIN_RESOURCE = 'SK_ContactedUs::contactedus_delete';

    /**
     * @var Filter
     */
    private $filter;

    /**
     * @var CollectionFactory
     */
    private $collectionFactory;

    /**
     * @var ContactedusRepositoryInterface
     */
    private $contactedusRepository;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * MassDelete constructor.
     *
     * @param Context $context
     * @param Filter $filter
     * @param CollectionFactory $collectionFactory
     * @param ContactedusRepositoryInterface $contactedusRepository
     * @param LoggerInterface $logger
     */
    public function __construct(
        Context $context,
        Filter $filter,
        CollectionFactory $collectionFactory,
        ContactedusRepositoryInterface $contactedusRepository,
        LoggerInterface $logger
    ) {
        $this->filter = $filter;
        $this->collectionFactory = $collectionFactory;
        $this->contactedusRepository = $contactedusRepository;
        $this->logger = $logger;
        parent::__construct($context);
    }

    /**
     * Delete Contacted Us massaction
     *
     * @return ResultInterface
     * @throws LocalizedException
     */
    public function execute()
    {
        /** @var Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();

        /** @var Collection $collection */
        $collection = $this->filter->getCollection($this->collectionFactory->create());

        try {
            $collectionSize = $collection->getSize();
            /** @var Contactedus $contactedus */
            foreach ($collection as $contactedus) {
                $this->contactedusRepository->deleteById($contactedus->getId());
            }
            $this->messageManager->addSuccessMessage(__('A total of %1 record(s) have been deleted.', $collectionSize));
        } catch (NoSuchEntityException $e) {
            $this->messageManager->addErrorMessage(__('There is no such Contacted Us entity to delete.'));
            $this->logger->critical($e);
        } catch (LocalizedException $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
            $this->logger->critical($e);
        } catch (\Exception $e) {
            $this->messageManager->addExceptionMessage($e, __('We can\'t mass delete the Contacted Us right now.'));
            $this->logger->critical($e);
        }

        return $resultRedirect->setPath('*/*/');
    }
}
