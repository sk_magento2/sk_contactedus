<?php
/**
 * @copyright: Copyright © 2019 Serhiy Kapitanets. All rights reserved.
 * @author   : Serhiy Kapitanets <kapitanets@gmail.com>
 */
namespace SK\ContactedUs\Model;

use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Model\AbstractModel;
use Magento\Framework\Model\Context;
use Magento\Framework\Registry;
use SK\ContactedUs\Api\Data\ContactedusInterface;
use SK\ContactedUs\Api\Data\ContactedusInterfaceFactory;
use SK\ContactedUs\Model\ResourceModel\Contactedus as ResourceModel;
use SK\ContactedUs\Model\ResourceModel\Contactedus\Collection;

/**
 * Class Contactedus
 * @package SK\ContactedUs\Model
 */
class Contactedus extends AbstractModel
{
    /**
     * Prefix of model events names
     *
     * @var string
     */
    protected $_eventPrefix = 'sk_contactedus';

    /**
     * @var DataObjectHelper
     */
    protected $dataObjectHelper;

    /**
     * @var ContactedusInterfaceFactory
     */
    protected $contactedusDataFactory;

    /**
     * @param Context $context
     * @param Registry $registry
     * @param ContactedusInterfaceFactory $contactedusDataFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param ResourceModel $resource
     * @param Collection $resourceCollection
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        ContactedusInterfaceFactory $contactedusDataFactory,
        DataObjectHelper $dataObjectHelper,
        ResourceModel $resource,
        Collection $resourceCollection,
        array $data = []
    ) {
        $this->contactedusDataFactory = $contactedusDataFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Retrieve contactedus model with contactedus data
     *
     * @return ContactedusInterface
     */
    public function getDataModel()
    {
        $contactedusData = $this->getData();

        $contactedusDataObject = $this->contactedusDataFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $contactedusDataObject,
            $contactedusData,
            ContactedusInterface::class
        );

        return $contactedusDataObject;
    }
}
