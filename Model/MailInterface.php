<?php
/**
 * @copyright: Copyright © 2019 Serhiy Kapitanets. All rights reserved.
 * @author   : Serhiy Kapitanets <kapitanets@gmail.com>
 */
namespace SK\ContactedUs\Model;

/**
 * Interface MailInterface
 * @package SK\ContactedUs\Model
 */
interface MailInterface
{
    /**
     * Send email from Contacted Us Answer form
     *
     * @param string $to To email address
     * @param array $variables Email template variables
     * @return void
     */
    public function send($to, array $variables);
}
