<?php
/**
 * @copyright: Copyright © 2019 Serhiy Kapitanets. All rights reserved.
 * @author   : Serhiy Kapitanets <kapitanets@gmail.com>
 */
namespace SK\ContactedUs\Model;

use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\ExtensibleDataObjectConverter;
use Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Exception\CouldNotSaveException as CouldNotSaveExceptionAlias;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Store\Model\StoreManagerInterface;
use SK\ContactedUs\Api\AnswerRepositoryInterface;
use SK\ContactedUs\Api\Data\AnswerInterface;
use SK\ContactedUs\Api\Data\AnswerSearchResultsInterfaceFactory;
use SK\ContactedUs\Model\ResourceModel\Answer as ResourceAnswer;
use SK\ContactedUs\Model\ResourceModel\Answer\CollectionFactory as AnswerCollectionFactory;

/**
 * Class AnswerRepository
 * @package SK\ContactedUs\Model
 */
class AnswerRepository implements AnswerRepositoryInterface
{
    /**
     * @var AnswerFactory
     */
    protected $answerFactory;

    /**
     * @var CollectionProcessorInterface
     */
    private $collectionProcessor;

    /**
     * @var ExtensibleDataObjectConverter
     */
    protected $extensibleDataObjectConverter;

    /**
     * @var AnswerCollectionFactory
     */
    protected $answerCollectionFactory;

    /**
     * @var DataObjectHelper
     */
    protected $dataObjectHelper;

    /**
     * @var JoinProcessorInterface
     */
    protected $extensionAttributesJoinProcessor;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var ResourceAnswer
     */
    protected $resource;

    /**
     * @var AnswerSearchResultsInterfaceFactory
     */
    protected $searchResultsFactory;

    /**
     * @var DataObjectProcessor
     */
    protected $dataObjectProcessor;

    /**
     * AnswerRepository constructor.
     *
     * @param ResourceAnswer $resource
     * @param AnswerFactory $answerFactory
     * @param AnswerCollectionFactory $answerCollectionFactory
     * @param AnswerSearchResultsInterfaceFactory $searchResultsFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param DataObjectProcessor $dataObjectProcessor
     * @param StoreManagerInterface $storeManager
     * @param CollectionProcessorInterface $collectionProcessor
     * @param JoinProcessorInterface $extensionAttributesJoinProcessor
     * @param ExtensibleDataObjectConverter $extensibleDataObjectConverter
     */
    public function __construct(
        ResourceAnswer $resource,
        AnswerFactory $answerFactory,
        AnswerCollectionFactory $answerCollectionFactory,
        AnswerSearchResultsInterfaceFactory $searchResultsFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        StoreManagerInterface $storeManager,
        CollectionProcessorInterface $collectionProcessor,
        JoinProcessorInterface $extensionAttributesJoinProcessor,
        ExtensibleDataObjectConverter $extensibleDataObjectConverter
    ) {
        $this->resource = $resource;
        $this->answerFactory = $answerFactory;
        $this->answerCollectionFactory = $answerCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->storeManager = $storeManager;
        $this->collectionProcessor = $collectionProcessor;
        $this->extensionAttributesJoinProcessor = $extensionAttributesJoinProcessor;
        $this->extensibleDataObjectConverter = $extensibleDataObjectConverter;
    }

    /**
     * @inheritDoc
     */
    public function save(AnswerInterface $answer)
    {
        $answerData = $this->extensibleDataObjectConverter->toNestedArray(
            $answer,
            [],
            AnswerInterface::class
        );

        $answerModel = $this->answerFactory->create()->setData($answerData);

        try {
            $this->resource->save($answerModel);
        } catch (\Exception $exception) {
            throw new CouldNotSaveExceptionAlias(__('Could not save the Answer: %1', $exception->getMessage()));
        }

        return $answerModel->getDataModel();
    }

    /**
     * @inheritDoc
     */
    public function getById($answerId)
    {
        $answer = $this->answerFactory->create();
        $this->resource->load($answer, $answerId);
        if (!$answer->getId()) {
            throw new NoSuchEntityException(__('Answer with id #%1 does not exist.', $answerId));
        }

        return $answer->getDataModel();
    }

    /**
     * @inheritDoc
     */
    public function getList(SearchCriteriaInterface $criteria)
    {
        $collection = $this->answerCollectionFactory->create();
        $this->extensionAttributesJoinProcessor->process($collection, AnswerInterface::class);
        $this->collectionProcessor->process($criteria, $collection);
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);

        $items = [];
        foreach ($collection as $model) {
            $items[] = $model->getDataModel();
        }

        $searchResults->setItems($items);
        $searchResults->setTotalCount($collection->getSize());

        return $searchResults;
    }
}
