<?php
/**
 * @copyright: Copyright © 2019 Serhiy Kapitanets. All rights reserved.
 * @author   : Serhiy Kapitanets <kapitanets@gmail.com>
 */
namespace SK\ContactedUs\Model;

use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Model\AbstractModel;
use Magento\Framework\Model\Context;
use Magento\Framework\Registry;
use SK\ContactedUs\Api\Data\AnswerInterface;
use SK\ContactedUs\Api\Data\AnswerInterfaceFactory;
use SK\ContactedUs\Model\ResourceModel\Answer as ResourceModel;
use SK\ContactedUs\Model\ResourceModel\Answer\Collection;

/**
 * Class Answer
 * @package SK\ContactedUs\Model
 */
class Answer extends AbstractModel
{
    /**
     * Prefix of model events names
     *
     * @var string
     */
    protected $_eventPrefix = 'sk_answer';

    /**
     * @var DataObjectHelper
     */
    protected $dataObjectHelper;

    /**
     * @var AnswerInterfaceFactory
     */
    protected $answerDataFactory;

    /**
     * @param Context $context
     * @param Registry $registry
     * @param AnswerInterfaceFactory $answerDataFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param ResourceModel $resource
     * @param Collection $resourceCollection
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        AnswerInterfaceFactory $answerDataFactory,
        DataObjectHelper $dataObjectHelper,
        ResourceModel $resource,
        Collection $resourceCollection,
        array $data = []
    ) {
        $this->answerDataFactory = $answerDataFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Retrieve answer model with answer data
     *
     * @return AnswerInterface
     */
    public function getDataModel()
    {
        $answerData = $this->getData();

        $answerDataObject = $this->answerDataFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $answerDataObject,
            $answerData,
            AnswerInterface::class
        );

        return $answerDataObject;
    }
}
