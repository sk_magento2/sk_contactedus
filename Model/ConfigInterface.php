<?php
/**
 * @copyright: Copyright © 2019 Serhiy Kapitanets. All rights reserved.
 * @author   : Serhiy Kapitanets <kapitanets@gmail.com>
 */
namespace SK\ContactedUs\Model;

/**
 * Interface ConfigInterface
 * @package SK\ContactedUs\Model
 */
interface ConfigInterface
{
    /**
     * Email template config path
     */
    const XML_PATH_EMAIL_TEMPLATE = 'contactedus/answer_email/email_template';

    /**
     * Return email template identifier
     *
     * @return string
     */
    public function emailTemplate();

    /**
     * Return email sender address
     *
     * @return string
     */
    public function emailSender();

    /**
     * Return email recipient address
     *
     * @return string
     */
    public function emailRecipient();
}
