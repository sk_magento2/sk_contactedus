<?php
/**
 * @copyright: Copyright © 2019 Serhiy Kapitanets. All rights reserved.
 * @author   : Serhiy Kapitanets <kapitanets@gmail.com>
 */
namespace SK\ContactedUs\Model\Data;

use Magento\Framework\Api\AbstractExtensibleObject;
use SK\ContactedUs\Api\Data\AnswerExtensionInterface;
use SK\ContactedUs\Api\Data\AnswerInterface;

/**
 * Class Answer
 * @package SK\ContactedUs\Model\Data
 */
class Answer extends AbstractExtensibleObject implements AnswerInterface
{
    /**
     * @inheritDoc
     */
    public function getAnswerId()
    {
        return $this->_get(self::ANSWER_ID);
    }

    /**
     * @inheritDoc
     */
    public function setAnswerId($answerId)
    {
        return $this->setData(self::ANSWER_ID, $answerId);
    }

    /**
     * @inheritDoc
     */
    public function getId()
    {
        return $this->getAnswerId();
    }

    /**
     * @inheritDoc
     */
    public function setId($id)
    {
        return $this->setAnswerId($id);
    }

    /**
     * @inheritDoc
     */
    public function getParentId()
    {
        return $this->_get(self::PARENT_ID);
    }

    /**
     * @inheritDoc
     */
    public function setParentId($parentId)
    {
        return $this->setData(self::PARENT_ID, $parentId);
    }

    /**
     * @inheritDoc
     */
    public function getAdminId()
    {
        return $this->_get(self::ADMIN_ID);
    }

    /**
     * @inheritDoc
     */
    public function setAdminId($adminId)
    {
        return $this->setData(self::ADMIN_ID, $adminId);
    }

    /**
     * @inheritDoc
     */
    public function getComment()
    {
        return $this->_get(self::COMMENT);
    }

    /**
     * @inheritDoc
     */
    public function setComment($comment)
    {
        return $this->setData(self::COMMENT, $comment);
    }

    /**
     * @inheritDoc
     */
    public function getCreationTime()
    {
        return $this->_get(self::CREATION_TIME);
    }

    /**
     * @inheritDoc
     */
    public function setCreationTime($creationTime)
    {
        return $this->setData(self::CREATION_TIME, $creationTime);
    }

    /**
     * @inheritDoc
     */
    public function getExtensionAttributes()
    {
        return $this->_getExtensionAttributes();
    }

    /**
     * @inheritDoc
     */
    public function setExtensionAttributes(AnswerExtensionInterface $extensionAttributes)
    {
        return $this->_setExtensionAttributes($extensionAttributes);
    }
}
