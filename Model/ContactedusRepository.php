<?php
/**
 * @copyright: Copyright © 2019 Serhiy Kapitanets. All rights reserved.
 * @author   : Serhiy Kapitanets <kapitanets@gmail.com>
 */
namespace SK\ContactedUs\Model;

use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\ExtensibleDataObjectConverter;
use Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Store\Model\StoreManagerInterface;
use SK\ContactedUs\Api\ContactedusRepositoryInterface;
use SK\ContactedUs\Api\Data\ContactedusInterface;
use SK\ContactedUs\Api\Data\ContactedusSearchResultsInterfaceFactory;
use SK\ContactedUs\Model\ResourceModel\Contactedus as ResourceContactedus;
use SK\ContactedUs\Model\ResourceModel\Contactedus\CollectionFactory as ContactedusCollectionFactory;

/**
 * Class ContactedusRepository
 * @package SK\ContactedUs\Model
 */
class ContactedusRepository implements ContactedusRepositoryInterface
{
    /**
     * @var ContactedusFactory
     */
    protected $contactedusFactory;

    /**
     * @var CollectionProcessorInterface
     */
    private $collectionProcessor;

    /**
     * @var ExtensibleDataObjectConverter
     */
    protected $extensibleDataObjectConverter;

    /**
     * @var ContactedusCollectionFactory
     */
    protected $contactedusCollectionFactory;

    /**
     * @var DataObjectHelper
     */
    protected $dataObjectHelper;

    /**
     * @var JoinProcessorInterface
     */
    protected $extensionAttributesJoinProcessor;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var ResourceContactedus
     */
    protected $resource;

    /**
     * @var ContactedusSearchResultsInterfaceFactory
     */
    protected $searchResultsFactory;

    /**
     * @var DataObjectProcessor
     */
    protected $dataObjectProcessor;

    /**
     * ContactedusRepository constructor.
     *
     * @param ResourceContactedus $resource
     * @param ContactedusFactory $contactedusFactory
     * @param ContactedusCollectionFactory $contactedusCollectionFactory
     * @param ContactedusSearchResultsInterfaceFactory $searchResultsFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param DataObjectProcessor $dataObjectProcessor
     * @param StoreManagerInterface $storeManager
     * @param CollectionProcessorInterface $collectionProcessor
     * @param JoinProcessorInterface $extensionAttributesJoinProcessor
     * @param ExtensibleDataObjectConverter $extensibleDataObjectConverter
     */
    public function __construct(
        ResourceContactedus $resource,
        ContactedusFactory $contactedusFactory,
        ContactedusCollectionFactory $contactedusCollectionFactory,
        ContactedusSearchResultsInterfaceFactory $searchResultsFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        StoreManagerInterface $storeManager,
        CollectionProcessorInterface $collectionProcessor,
        JoinProcessorInterface $extensionAttributesJoinProcessor,
        ExtensibleDataObjectConverter $extensibleDataObjectConverter
    ) {
        $this->resource = $resource;
        $this->contactedusFactory = $contactedusFactory;
        $this->contactedusCollectionFactory = $contactedusCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->storeManager = $storeManager;
        $this->collectionProcessor = $collectionProcessor;
        $this->extensionAttributesJoinProcessor = $extensionAttributesJoinProcessor;
        $this->extensibleDataObjectConverter = $extensibleDataObjectConverter;
    }

    /**
     * {@inheritdoc}
     */
    public function save(ContactedusInterface $contactedus)
    {
        $contactedusData = $this->extensibleDataObjectConverter->toNestedArray(
            $contactedus,
            [],
            ContactedusInterface::class
        );

        $contactedusModel = $this->contactedusFactory->create()->setData($contactedusData);

        try {
            $this->resource->save($contactedusModel);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__('Could not save the Contacted Us: %1', $exception->getMessage()));
        }

        return $contactedusModel->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function getById($contactedusId)
    {
        $contactedus = $this->contactedusFactory->create();
        $this->resource->load($contactedus, $contactedusId);
        if (!$contactedus->getId()) {
            throw new NoSuchEntityException(__('Contacted Us with id #%1 does not exist.', $contactedusId));
        }

        return $contactedus->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function getList(SearchCriteriaInterface $criteria)
    {
        $collection = $this->contactedusCollectionFactory->create();
        $this->extensionAttributesJoinProcessor->process($collection, ContactedusInterface::class);
        $this->collectionProcessor->process($criteria, $collection);
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);

        $items = [];
        foreach ($collection as $model) {
            $items[] = $model->getDataModel();
        }

        $searchResults->setItems($items);
        $searchResults->setTotalCount($collection->getSize());

        return $searchResults;
    }

    /**
     * {@inheritdoc}
     */
    public function delete(ContactedusInterface $contactedus)
    {
        try {
            $contactedusModel = $this->contactedusFactory->create();
            $this->resource->load($contactedusModel, $contactedus->getId());
            $this->resource->delete($contactedusModel);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__('Could not delete the Contacted Us: %1', $exception->getMessage()));
        }

        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($id)
    {
        return $this->delete($this->getById($id));
    }
}
