<?php
/**
 * @copyright: Copyright © 2019 Serhiy Kapitanets. All rights reserved.
 * @author   : Serhiy Kapitanets <kapitanets@gmail.com>
 */
namespace SK\ContactedUs\Model;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Contact\Model\ConfigInterface as ContactConfigInterface;

/**
 * Class Config
 * @package SK\ContactedUs\Model
 */
class Config implements ConfigInterface
{
    /**
     * @var ContactConfigInterface
     */
    private $contactConfig;
    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @param ContactConfigInterface $contactConfig
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        ContactConfigInterface $contactConfig,
        ScopeConfigInterface $scopeConfig
    ) {
        $this->contactConfig = $contactConfig;
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * {@inheritdoc}
     */
    public function emailTemplate()
    {
        return $this->scopeConfig->getValue(
            ConfigInterface::XML_PATH_EMAIL_TEMPLATE,
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * {@inheritdoc}
     */
    public function emailSender()
    {
        return $this->contactConfig->emailSender();
    }

    /**
     * {@inheritdoc}
     */
    public function emailRecipient()
    {
        return $this->contactConfig->emailRecipient();
    }
}
