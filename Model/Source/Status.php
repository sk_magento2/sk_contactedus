<?php
/**
 * @copyright: Copyright © 2019 Serhiy Kapitanets. All rights reserved.
 * @author   : Serhiy Kapitanets <kapitanets@gmail.com>
 */
namespace SK\ContactedUs\Model\Source;

use Magento\Framework\Data\OptionSourceInterface;

/**
 * Class Status
 * @package SK\ContactedUs\Model\Source
 */
class Status implements OptionSourceInterface
{
    /**
     * Get options
     *
     * @return array
     */
    public function toOptionArray()
    {
        $options = [
            1 => [
                'label' => 'New',
                'value' => 1,
            ],
            2 => [
                'label' => 'Answered',
                'value' => 2,
            ],
            3 => [
                'label' => 'Resolved',
                'value' => 3,
            ],
        ];

        return $options;
    }
}
