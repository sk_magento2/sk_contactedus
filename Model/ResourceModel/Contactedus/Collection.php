<?php
/**
 * @copyright: Copyright © 2019 Serhiy Kapitanets. All rights reserved.
 * @author   : Serhiy Kapitanets <kapitanets@gmail.com>
 */
namespace SK\ContactedUs\Model\ResourceModel\Contactedus;

use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use SK\ContactedUs\Model\Contactedus as ModelContactedus;
use SK\ContactedUs\Model\ResourceModel\Contactedus as ResourceModelContactedus;

/**
 * Class Collection
 * @package SK\ContactedUs\Model\ResourceModel\Contactedus
 */
class Collection extends AbstractCollection
{
    /**
     * Define resource model
     *
     * @return void
     * @throws LocalizedException
     */
    protected function _construct()
    {
        $this->_init(ModelContactedus::class, ResourceModelContactedus::class);
        $this->_setIdFieldName($this->getResource()->getIdFieldName());
    }
}
