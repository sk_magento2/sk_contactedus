<?php
/**
 * @copyright: Copyright © 2019 Serhiy Kapitanets. All rights reserved.
 * @author   : Serhiy Kapitanets <kapitanets@gmail.com>
 */
namespace SK\ContactedUs\Model\ResourceModel\Answer;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use SK\ContactedUs\Model\Answer as ModelAnswer;
use SK\ContactedUs\Model\ResourceModel\Answer as ResourceModelAnswer;

/**
 * Class Collection
 * @package SK\ContactedUs\Model\ResourceModel\Answer
 */
class Collection extends AbstractCollection
{
    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(ModelAnswer::class, ResourceModelAnswer::class);
    }
}
