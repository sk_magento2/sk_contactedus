<?php
/**
 * @copyright: Copyright © 2019 Serhiy Kapitanets. All rights reserved.
 * @author   : Serhiy Kapitanets <kapitanets@gmail.com>
 */
namespace SK\ContactedUs\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

/**
 * Interface ContactedusSearchResultsInterface
 * @package SK\ContactedUs\Api\Data
 */
interface ContactedusSearchResultsInterface extends SearchResultsInterface
{
    /**
     * Get Contactedus list.
     *
     * @return ContactedusInterface[]
     */
    public function getItems();

    /**
     * Set Contactedus list.
     *
     * @param ContactedusInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
