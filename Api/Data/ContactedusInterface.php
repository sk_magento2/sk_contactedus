<?php
/**
 * @copyright: Copyright © 2019 Serhiy Kapitanets. All rights reserved.
 * @author   : Serhiy Kapitanets <kapitanets@gmail.com>
 */
namespace SK\ContactedUs\Api\Data;

use Magento\Framework\Api\ExtensibleDataInterface;

/**
 * Interface ContactedusInterface
 * @package SK\ContactedUs\Api\Data
 */
interface ContactedusInterface extends ExtensibleDataInterface
{
    const ENTITY_ID = 'entity_id';
    const NAME = 'name';
    const EMAIL = 'email';
    const PHONE = 'phone';
    const COMMENT = 'comment';
    const CREATION_TIME = 'creation_time';
    const STATUS = 'status';

    /**
     * Get Entity Id
     *
     * @return int|null
     */
    public function getEntityId();

    /**
     * Set Entity Id
     *
     * @param int $entityId
     * @return ContactedusInterface
     */
    public function setEntityId($entityId);

    /**
     * Get Entity Id
     *
     * @return int|null
     */
    public function getId();

    /**
     * Set Entity Id
     *
     * @param int $entityId
     * @return ContactedusInterface
     */
    public function setId($entityId);

    /**
     * Get Name
     *
     * @return string|null
     */
    public function getName();

    /**
     * Set Name
     *
     * @param string $name
     * @return ContactedusInterface
     */
    public function setName($name);

    /**
     * Get Email
     *
     * @return string|null
     */
    public function getEmail();

    /**
     * Set Email
     *
     * @param string $email
     * @return ContactedusInterface
     */
    public function setEmail($email);

    /**
     * Get Phone
     *
     * @return string|null
     */
    public function getPhone();

    /**
     * Set Phone
     *
     * @param string $phone
     * @return ContactedusInterface
     */
    public function setPhone($phone);

    /**
     * Get Comment
     *
     * @return string|null
     */
    public function getComment();

    /**
     * Set Comment
     *
     * @param string $comment
     * @return ContactedusInterface
     */
    public function setComment($comment);

    /**
     * Get Creation Time
     *
     * @return string|null
     */
    public function getCreationTime();

    /**
     * Set Creation Time
     *
     * @param string $creationTime
     * @return ContactedusInterface
     */
    public function setCreationTime($creationTime);

    /**
     * Get Status
     *
     * @return int|null
     */
    public function getStatus();

    /**
     * Set Status
     *
     * @param int $status
     * @return ContactedusInterface
     */
    public function setStatus($status);

    /**
     * Retrieve existing extension attributes object or create a new one.
     *
     * @return \SK\ContactedUs\Api\Data\ContactedusExtensionInterface|null
     */
    public function getExtensionAttributes();

    /**
     * Set an extension attributes object.
     *
     * @param \SK\ContactedUs\Api\Data\ContactedusExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(\SK\ContactedUs\Api\Data\ContactedusExtensionInterface $extensionAttributes);
}
