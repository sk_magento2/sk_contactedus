<?php
/**
 * @copyright: Copyright © 2019 Serhiy Kapitanets. All rights reserved.
 * @author   : Serhiy Kapitanets <kapitanets@gmail.com>
 */
namespace SK\ContactedUs\Api\Data;

use Magento\Framework\Api\ExtensibleDataInterface;

/**
 * Interface AnswerInterface
 * @package SK\ContactedUs\Api\Data
 */
interface AnswerInterface extends ExtensibleDataInterface
{
    const ANSWER_ID = 'answer_id';
    const PARENT_ID = 'parent_id';
    const ADMIN_ID = 'admin_id';
    const COMMENT = 'comment';
    const CREATION_TIME = 'creation_time';

    /**
     * Get Answer Id
     *
     * @return int|null
     */
    public function getAnswerId();

    /**
     * Set Answer Id
     *
     * @param int $answerId
     * @return AnswerInterface
     */
    public function setAnswerId($answerId);

    /**
     * Get Answer Id
     *
     * @return int|null
     */
    public function getId();

    /**
     * Set Answer Id
     *
     * @param int $answerId
     * @return AnswerInterface
     */
    public function setId($answerId);

    /**
     * Get Parent Id
     *
     * @return int|null
     */
    public function getParentId();

    /**
     * Set Parent Id
     *
     * @param int $parentId
     * @return AnswerInterface
     */
    public function setParentId($parentId);

    /**
     * Get Admin Id
     *
     * @return int|null
     */
    public function getAdminId();

    /**
     * Set Admin Id
     *
     * @param int $adminId
     * @return AnswerInterface
     */
    public function setAdminId($adminId);

    /**
     * Get Comment
     *
     * @return string|null
     */
    public function getComment();

    /**
     * Set Comment
     *
     * @param string $comment
     * @return AnswerInterface
     */
    public function setComment($comment);

    /**
     * Get Creation Time
     *
     * @return string|null
     */
    public function getCreationTime();

    /**
     * Set Creation Time
     *
     * @param string $creationTime
     * @return AnswerInterface
     */
    public function setCreationTime($creationTime);

    /**
     * Retrieve existing extension attributes object or create a new one.
     *
     * @return \SK\ContactedUs\Api\Data\AnswerExtensionInterface|null
     */
    public function getExtensionAttributes();

    /**
     * Set an extension attributes object.
     *
     * @param \SK\ContactedUs\Api\Data\AnswerExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(\SK\ContactedUs\Api\Data\AnswerExtensionInterface $extensionAttributes);
}
