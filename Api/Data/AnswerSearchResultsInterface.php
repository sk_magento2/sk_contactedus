<?php
/**
 * @copyright: Copyright © 2019 Serhiy Kapitanets. All rights reserved.
 * @author   : Serhiy Kapitanets <kapitanets@gmail.com>
 */
namespace SK\ContactedUs\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

/**
 * Interface AnswerSearchResultsInterface
 * @package SK\ContactedUs\Api\Data
 */
interface AnswerSearchResultsInterface extends SearchResultsInterface
{
    /**
     * Get Answer list.
     *
     * @return AnswerInterface[]
     */
    public function getItems();

    /**
     * Set Answer list.
     *
     * @param AnswerInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
