<?php
/**
 * @copyright: Copyright © 2019 Serhiy Kapitanets. All rights reserved.
 * @author   : Serhiy Kapitanets <kapitanets@gmail.com>
 */
namespace SK\ContactedUs\Api\Data;

use Magento\Framework\Api\ExtensionAttributesInterface;

/**
 * ExtensionInterface class for @see \SK\ContactedUs\Api\Data\AnswerInterface
 */
interface AnswerExtensionInterface extends ExtensionAttributesInterface
{
}
