<?php
/**
 * @copyright: Copyright © 2019 Serhiy Kapitanets. All rights reserved.
 * @author   : Serhiy Kapitanets <kapitanets@gmail.com>
 */
namespace SK\ContactedUs\Api;

use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use SK\ContactedUs\Api\Data\AnswerInterface;
use SK\ContactedUs\Api\Data\AnswerSearchResultsInterface;

/**
 * Interface AnswerRepositoryInterface
 * @package SK\ContactedUs\Api
 */
interface AnswerRepositoryInterface
{
    /**
     * Save Answer
     *
     * @param AnswerInterface $answer
     * @return AnswerInterface
     * @throws LocalizedException
     */
    public function save(AnswerInterface $answer);

    /**
     * Retrieve Answer
     *
     * @param string $id
     * @return AnswerInterface
     * @throws NoSuchEntityException
     */
    public function getById($id);

    /**
     * Retrieve Answer matching the specified criteria.
     *
     * @param SearchCriteriaInterface $searchCriteria
     * @return AnswerSearchResultsInterface
     * @throws LocalizedException
     */
    public function getList(SearchCriteriaInterface $searchCriteria);
}