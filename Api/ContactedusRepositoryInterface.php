<?php
/**
 * @copyright: Copyright © 2019 Serhiy Kapitanets. All rights reserved.
 * @author   : Serhiy Kapitanets <kapitanets@gmail.com>
 */
namespace SK\ContactedUs\Api;

use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use SK\ContactedUs\Api\Data\ContactedusInterface;
use SK\ContactedUs\Api\Data\ContactedusSearchResultsInterface;

/**
 * Interface ContactedusRepositoryInterface
 * @package SK\ContactedUs\Api
 */
interface ContactedusRepositoryInterface
{
    /**
     * Save Contactedus
     *
     * @param ContactedusInterface $contactedus
     * @return ContactedusInterface
     * @throws LocalizedException
     */
    public function save(ContactedusInterface $contactedus);

    /**
     * Retrieve Contactedus
     *
     * @param string $id
     * @return ContactedusInterface
     * @throws NoSuchEntityException
     */
    public function getById($id);

    /**
     * Retrieve Contactedus matching the specified criteria.
     *
     * @param SearchCriteriaInterface $searchCriteria
     * @return ContactedusSearchResultsInterface
     * @throws LocalizedException
     */
    public function getList(SearchCriteriaInterface $searchCriteria);

    /**
     * Delete Contactedus
     *
     * @param ContactedusInterface $contactedus
     * @return bool true on success
     * @throws LocalizedException
     */
    public function delete(ContactedusInterface $contactedus);

    /**
     * Delete Contactedus by ID
     *
     * @param string $id
     * @return bool true on success
     * @throws NoSuchEntityException
     * @throws LocalizedException
     */
    public function deleteById($id);
}
