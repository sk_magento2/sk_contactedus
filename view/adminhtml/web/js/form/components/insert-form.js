/**
 * @copyright: Copyright © 2019 Serhiy Kapitanets. All rights reserved.
 * @author   : Serhiy Kapitanets <kapitanets@gmail.com>
 */

define([
    'Magento_Ui/js/form/components/insert-form'
], function (Insert) {
    'use strict';

    return Insert.extend({
        defaults: {
            listens: {
                responseData: 'onResponse'
            },
            modules: {
                answerListing: '${ $.answerListingProvider }',
                answerModal: '${ $.answerModalProvider }'
            }
        },

        /**
         * Close modal, reload answer listing
         *
         * @param {Object} responseData
         */
        onResponse: function (responseData) {
            if (!responseData.error) {
                this.answerModal().closeModal();
                this.answerListing().reload({
                    refresh: true
                });
            }
        }
    });
});
